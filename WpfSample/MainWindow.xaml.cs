﻿using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;

namespace WpfSample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Empregado newEmpregado { get; set; }

        wpfnatEntities context = new wpfnatEntities();

        CollectionViewSource emprViewSource;

        public MainWindow()
        {
            InitializeComponent();
            newEmpregado = new Empregado();
            emprViewSource = ((CollectionViewSource)
                (FindResource("empregadoViewSource")));

            DataContext = this;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            context.Empregadoes.Load();
            emprViewSource.Source = context.Empregadoes.Local;
        }

        private void LastCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            emprViewSource.View.MoveCurrentToLast();
        }

        private void PreviousCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            emprViewSource.View.MoveCurrentToPrevious();
        }

        private void NextCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            emprViewSource.View.MoveCurrentToNext();
        }

        private void FirstCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            emprViewSource.View.MoveCurrentToFirst();
        }

        private void DeleteCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            if (MessageBox.Show("Do you want to delete this employee?", "Delete", MessageBoxButton.YesNoCancel) == MessageBoxResult.Yes)
            {
                int pos = emprViewSource.View.CurrentPosition;
                ((ListCollectionView)emprViewSource.View).RemoveAt(pos);
                context.SaveChanges();
                emprViewSource.View.Refresh();
            }
        }

        private void empregado_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            Application.Current.Dispatcher.InvokeAsync(new Action(() =>
            {
                Empregado emp = e.Row.DataContext as Empregado;
                if (e.EditAction == DataGridEditAction.Commit)
                {
                    if (e.Row.IsNewItem)
                    {
                        context.Empregadoes.Local.Insert(context.Empregadoes.Local.Count, emp);
                        ((ListCollectionView)emprViewSource.View).CommitNew();
                        ((ListCollectionView)emprViewSource.View).CommitNew();
                    }
                    else
                    {
                        var updated = context.Empregadoes.Find(new object[] { emp.id });

                        if (updated != null)
                        {
                            updated.FirstName = emp.FirstName;
                            updated.LastName = emp.LastName;
                            updated.PhoneNumber = emp.PhoneNumber;
                            updated.Email = emp.Email;
                            ((ListCollectionView)emprViewSource.View).CommitEdit();
                            ((ListCollectionView)emprViewSource.View).CommitEdit();
                        }
                    }
                }
                else
                {
                    if (e.Row.IsNewItem)
                    {
                        ((ListCollectionView)emprViewSource.View).CancelNew();
                        ((ListCollectionView)emprViewSource.View).CancelNew();
                        ((ListCollectionView)emprViewSource.View).Remove(emp);
                    }
                    else
                    {
                        ((ListCollectionView)emprViewSource.View).CancelEdit();
                        ((ListCollectionView)emprViewSource.View).CancelEdit();
                    }
                }
                try
                {
                    context.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var eve in ex.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                }
                emprViewSource.View.Refresh();
                emprViewSource.View.MoveCurrentTo(emp);
            }), DispatcherPriority.ContextIdle);

            return;
        }
    }
}
