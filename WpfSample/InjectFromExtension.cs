﻿using Omu.ValueInjecter;
using Omu.ValueInjecter.Injections;
using System.Collections.Generic;

namespace Focus.Util.Services
{
    public static class InjectFromExtension
    {   
        public static ICollection<TTo> InjectFrom<TFrom, TTo>(this ICollection<TTo> to, params IEnumerable<TFrom>[] sources) where TTo : new()
        {
            foreach (var from in sources)
            {
                foreach (var source in from)
                {
                    var target = new TTo();
                    target.InjectFrom(source);
                    to.Add(target);
                }
            }
            return to;
        }

        /// <summary>
        /// UnflatLoopInjection, matches flat properties to unflat ( abc => a.b.c ); override
        /// SetValue to control the how the value is set ( do type casting, ignore setting
        /// in certain scenarios etc. ); override Match to control unflat target checking;
        /// </summary>
        public static ICollection<TTo> UnFlatInjectFrom<TFrom, TTo>(this ICollection<TTo> to, params IEnumerable<TFrom>[] sources) where TTo : new()
        {
            foreach (var from in sources)
            {
                foreach (var source in from)
                {
                    var target = new TTo();
                    target.InjectFrom<UnflatLoopInjection>(source);
                    to.Add(target);
                }
            }
            return to;
        }

        /// <summary>
        /// FlatLoopInjection, matches unflat properties to flat ( a.b.c => abc ) override
        /// SetValue to control the how the value is set ( do type casting, ignore setting
        /// in certain scenarios etc. ) override Match to control unflat target checking
        /// </summary>
        public static ICollection<TTo> FlatInjectFrom<TFrom, TTo>(this ICollection<TTo> to, params IEnumerable<TFrom>[] sources) where TTo : new()
        {
            foreach (var from in sources)
            {
                foreach (var source in from)
                {
                    var target = new TTo();
                    target.InjectFrom<FlatLoopInjection>(source);
                    to.Add(target);
                }
            }
            return to;
        }

        //
        // Resumo:
        //     Injects values from source to target
        //
        // Parâmetros:
        //   target:
        //     target where the value is going to be injected
        //
        //   source:
        //     source from where the value is taken
        //
        // Parâmetros de Tipo:
        //   T:
        //     ValueInjection used
        //
        // Devoluções:
        //     the modified target
        public static object InjectFrom<T>(object target, object source) where T : IValueInjection, new() {
            return InjectFrom<T>(target, source);
        }
        //
        // Resumo:
        //     Injects values from source to target
        //
        // Parâmetros:
        //   target:
        //     target where the value is going to be injected
        //
        //   injection:
        //     ValueInjection used
        //
        //   source:
        //     source from where the value is taken
        //
        // Devoluções:
        //     the modified target
        public static object InjectFrom(object target, IValueInjection injection, object source) {
            return InjectFrom(target, injection, source);
        }
        //
        // Resumo:
        //     Injects values into the target
        //
        // Parâmetros:
        //   target:
        //     target where the value is going to be injected
        //
        // Parâmetros de Tipo:
        //   T:
        //     ValueInjection(INoSourceValueInjection) used for that
        //
        // Devoluções:
        //     the modified target
        public static object InjectFrom<T>(object target) where T : INoSourceInjection, new() {
            return InjectFrom<T>(target);
        }
        //
        // Resumo:
        //     Injects value into target without source
        //
        // Parâmetros:
        //   target:
        //     the target where the value is going to be injected
        //
        //   injection:
        //     the injection(INoSourceValueInjection) used to inject value
        //
        // Devoluções:
        //     the modified target
        public static object InjectFrom(object target, INoSourceInjection injection) {
            return InjectFrom(target, injection);
        }
        //
        // Resumo:
        //     Inject properties with exact same name and type
        public static object InjectFrom(object target, object source) {
            return InjectFrom(target, source);
        }
    }
}
